#!/bin/sh
if [ "$#" -ne 2 ]; then
    echo "$0 <accesskey> <secret>"
    exit
fi

export AWS_ACCESS_KEY_ID=$1
export AWS_SECRET_ACCESS_KEY=$2
aws s3 sync public s3://www.curlbalmoral.com
